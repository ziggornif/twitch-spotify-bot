# twitch-spotify-bot

## Create spotify app

Go to https://developer.spotify.com/dashboard/applications to create your twitch bot app.

To do this part, follow the spotify official guideline : https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app

## Deployment

### Run nodejs app

Clone the project.
```sh
git clone https://gitlab.com/ziggornif/twitch-spotify-bot
```

Install node modules.
```sh
npm ci
```

Run application.
```sh
CLIENT_ID=clientid CLIENT_SECRET=clientsecret CALLBACK_URL=http://yoururl/callback node index.js
```

### Docker 

```sh
docker run -d -p 8080:8080 -e CLIENT_ID=clientid -e CLIENT_SECRET=clientsecret -e CALLBACK_URL=http://yoururl/callback registry.gitlab.com/ziggornif/twitch-spotify-bot:master
```

### Docker-compose

```yaml
version: "3.3"
services:
  api:
    image: registry.gitlab.com/ziggornif/twitch-spotify-bot:master
    ports:
      - "8082:8080"
    environment:
      - CLIENT_ID=clientid
      - CLIENT_SECRET=clientsecret
      - CALLBACK_URL=http://yoururl/callback 
```

```sh
docker-compose up -d
```

## Register application

Open this url with your values :

```
https://accounts.spotify.com/authorize?client_id=clientid&response_type=code&redirect_uri=yoururl/callback&scope=user-read-currently-playing%20user-read-playback-state&state=twitch-spotify-bot&show_dialog=false
```

## Spotify overlay item

![](./assets/overlay.png)

Spotify overlay item is available from `/current-play`.

You can add it with a new browser source.

### Example with OBS Studio 

Sources > Add > Browser

![](./assets/addsource.png)

### Use custom text color

You can change text color by using custom CSS color attribute.

Text black example
```css
content {
    color: #000000;
}
```

## Moobot command (API call)

JSON current spofity playing song is available from `/api/current-play`.

Response example :
```js
{
  "name": "Hit the Floor",
  "artist": "Various Artists, DJ Electrohead",
  "image": "https://i.scdn.co/image/ab67616d0000b27322d6861ac8aad61aa626a29e",
  "duration": 210733,
  "progress": 103446
}
```

### Moobot chat command setup

Go to https://moo.bot website and connect with your twitch account.

On the left pannel, click on `Custom commands` item.

On the widget, set the bot name and click on create.

On the command settings, set this configuration :

![](./assets/moobot.png)

- Response : URL fetch - JSON Object #1 - URL fetch - JSON Object #2
- URL to fetch : yoururl/api/current-play
- URL response type : JSON
- Request method : GET
- JSON pointer of object #1 : /name
- JSON pointer of object #2 : /artist

Your command is now ready to use, try it on your chat with `!yourcommand`.