module.exports = {
  clientId: process.env.CLIENT_ID || 'clientId',
  clientSecret: process.env.CLIENT_SECRET || 'secret',
  redirectUri: process.env.CALLBACK_URL || 'http://example.com/callback',
};
