FROM node:14-alpine AS dependencies
WORKDIR /src
ADD package.json package-lock.json /src/
RUN npm ci --production

FROM node:14-alpine
LABEL maintainer="ziggornif@gmail.com"

WORKDIR /src
COPY --from=dependencies /src .

COPY . /src/

ENV NODE_ENV 'production'

EXPOSE 8080

USER node

CMD ["node", "index.js"]
