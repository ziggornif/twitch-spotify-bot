const assert = require('assert');
const SpotifyWebApi = require('spotify-web-api-node');
const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const config = require('config');

const spotifyApi = new SpotifyWebApi({
  clientId: config.clientId,
  clientSecret: config.clientSecret,
  redirectUri: config.redirectUri,
});

function trackParser(spotifyTrack) {
  assert.ok(spotifyTrack, 'spotifyTrack parameter is mandatory');
  return {
    name: spotifyTrack?.item?.name,
    artist: spotifyTrack?.item?.artists.map((artist) => artist.name).join(', '),
    image: spotifyTrack?.item?.album?.images[0]?.url,
    duration: spotifyTrack?.item?.duration_ms,
    progress: spotifyTrack?.progress_ms,
  };
}

/**
 * Get spotify current playing track
 * @returns {object} track
 */
async function getCurrentPlayingTrack() {
  const { body } = await spotifyApi.getMyCurrentPlayingTrack();
  assert.ok(body, 'Response body is missing');

  return trackParser(body);
}

async function refreshToken() {
  const { body } = await spotifyApi.refreshAccessToken();
  spotifyApi.setAccessToken(body.access_token);
}

async function run() {
  const app = express();
  app.use(cors());

  app.engine('handlebars', exphbs());
  app.set('view engine', 'handlebars');

  app.use(helmet({ contentSecurityPolicy: false }));
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: false,
    })
  );

  app.get('/callback', async (req, res) => {
    const data = await spotifyApi.authorizationCodeGrant(req.query.code);
    spotifyApi.setAccessToken(data.body.access_token);
    spotifyApi.setRefreshToken(data.body.refresh_token);
    res.redirect('/current-play');
  });

  app.get('/current-play', async (req, res) => {
    try {
      await refreshToken().catch((err) => {
        console.log(err);
      });
      const track = await getCurrentPlayingTrack();
      res.render('home', track);
    } catch (error) {
      res.status(500).send('Error');
    }
  });

  app.get('/api/current-play', async (req, res) => {
    try {
      const track = await getCurrentPlayingTrack();
      res.json(track);
    } catch (error) {
      res.status(500).json({
        message: 'Error',
      });
    }
  });

  const server = app.listen(8080, () => {
    const { port } = server.address();
    console.log(`[twitch-spotify-bot] 🚀 server started and available on http://localhost:${port}`);
  });
}

run();
